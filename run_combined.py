#!/usr/bin/env python3


import os


for i in range(100):
    os.system("python3 classifier_combined.py "+str(i)+" >> out.txt")

f = open('out.txt')
l = [float(i) for i in f.readlines()]
f.close()

print(sum(l) / len(l) * 100)
