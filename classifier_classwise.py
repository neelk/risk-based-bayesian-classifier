#!/usr/bin/env python3


import sys

import modelmanager as mm
import datamanager as dm


def main(argv):

    try:
        seed = int(argv[1])
    except IndexError:
        seed = 0

    (trainNatFV, trainInterFV, trainFV,
     testNatFV, testInterFV, testFV) = dm.getData(seed=seed)

    # normNat = mm.MultivariateNormal(trainNatFV)
    # normInter = mm.MultivariateNormal(trainInterFV)

    classifier = mm.BayesRiskClassifier(data=(trainInterFV,
                                              trainNatFV),
                                        loss=mm.ZeroOneLoss,
                                        distribution=mm.MultivariateNormal)

    ntotal = 0
    nmissclass = 0
    for x in testNatFV.transpose():
        if classifier.classify(x) == 0:
            nmissclass += 1
        ntotal += 1

    # print(missclass, total)


    itotal = 0
    imissclass = 0
    for x in testInterFV.transpose():
        if classifier.classify(x) == 1:
            imissclass += 1
        itotal += 1

    # print(missclass, total)

    print(imissclass/itotal, nmissclass/ntotal)

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
