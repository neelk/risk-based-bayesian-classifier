#!/usr/bin/env python3


import math
import numpy


def mean(data):

    return numpy.float64(sum(data)/len(data))


def stddev(data):

    m = mean(data)
    df = len(data) - 1

    s = 0
    for i in range(len(data)):
        s += math.pow(data[i] - m, 2)

    return numpy.float64(math.sqrt(s / df))


def covariance(v1, v2):

    if len(v1) != len(v2):
        raise ValueError('vectors of unequal length')

    m1 = mean(v1)
    m2 = mean(v2)

    s = 0
    for i in range(len(v1)):
        s += (v1[i] - m1) * (v2[i] - m2)

    return numpy.float64(s / len(v1))


class GeneralLoss(object):

    def __init__(self, lossmat):
        self.__lossmat = lossmat

    def loss(self, row, col):
        return self.__lossmat[row][col]


class ZeroOneLoss(GeneralLoss):

    def __init__(self, dim):
        self.__lossmat = [[1 for i in range(dim)] for j in range(dim)]
        for i in range(dim):
            self.__lossmat[i][i] = 0

        self.__genloss = GeneralLoss.__init__(self,  self.__lossmat)


class MultivariateNormal(object):


    def __init__(self, data):

        self.n_fields = len(data)
        self.n_datapoints = len(data[0])
        self.meanvector = numpy.zeros(shape=(self.n_fields,),
                                      dtype=numpy.float64)
        self.covmat = numpy.zeros(shape=(self.n_fields,
                                         self.n_fields),
                                  dtype=numpy.float64)
        self.__setvals(data)


    def __setvals(self, data):

        for i in range(self.n_fields):
            self.meanvector[i] = mean(data[i])

        for i in range(self.n_fields):
            for j in range(self.n_fields):
                self.covmat[i][j] = covariance(data[i], data[j])


    def likelihood(self, x):
        diff = x - self.meanvector
        diff = numpy.array([diff]).transpose()
        power = numpy.matmul(numpy.matmul(diff.transpose(),
                             numpy.linalg.inv(self.covmat)),
                             diff)[0][0] * (-1/2)
        exp = math.exp(power)
        const = 1 / math.sqrt(math.pow(2 * math.pi, self.n_fields) *
                              numpy.linalg.det(self.covmat))
        return const * exp


'''
Any simple loss function must have:
 -dims
 -method loss

Any general loss function must have:
 -lossmat
 -method loss

Any distribution must have:
 -method likelihood
'''


class BayesRiskClassifier(object):


    def __init__(self, **kwargs):

        '''
            data: sequence of data-points for every class
        '''

        try:

            self.__lossfn = kwargs['loss'](dim=len(kwargs['data']))
            self.__dists = [kwargs['distribution'](vec)
                              for vec in kwargs['data']]

            self.__nclass = len(kwargs['data'])

            for train_i in kwargs['data']:
                for i in range(len(train_i)):
                    if len(train_i[i]) != len(train_i[0]):
                        raise ValueError('invalid train data dimensions \
in class ' + i)

            self.__classprobs = []
            self.__totaldata = sum([len(i[0]) for i in kwargs['data']])
            for i in range(self.__nclass):
                self.__classprobs.append(len(kwargs['data'][i][0])
                                                 / self.__totaldata)

        except KeyError as e:
            raise ValueError('parameter ' + e + ' not specified')


    def __classprob(self, classno):
        return self.__classprobs[classno]


    def __getrisk(self, x):

        risks = []
        for cl in range(self.__nclass):
            risk = 0
            for r in range(self.__nclass):
                risk += self.__lossfn.loss(cl, r) * \
                        self.__dists[r].likelihood(x) * \
                        self.__classprob(r)
            risks.append(risk)

        return risks


    def classify(self, x):
        risks = self.__getrisk(x)
        classified = risks.index(min(risks))
        return classified
