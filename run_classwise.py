#!/usr/bin/env python3


import os


for i in range(100):
    os.system("python3 classifier_classwise.py "+str(i)+" >> out.txt")

li = []
ln = []
f = open('out.txt')
for l in f.readlines():
    vals = [float(i) for i in l.split()]
    try:
        li.append(vals[0])
        ln.append(vals[1])
    except:
        print(vals)
f.close()

print('International Journals Misclassification:', sum(li) / len(li) * 100)
print('National Journals Misclassification:', sum(ln) / len(ln) * 100)
