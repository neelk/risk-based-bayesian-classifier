#!/usr/bin/env python3


import sys

import modelmanager as mm
import datamanager as dm


def main(argv):

    try:
        seed = int(argv[1])
    except IndexError:
        seed = 0

    (trainNatFV, trainInterFV, trainFV,
     testNatFV, testInterFV, testFV) = dm.getData(seed=seed)

    normNat = mm.MultivariateNormal(trainNatFV)
    normInter = mm.MultivariateNormal(trainInterFV)

    total = 0
    missclass = 0
    for x in testNatFV.transpose():
        if normNat.likelihood(x) > normInter.likelihood(x):
            pass
        else:
            missclass += 1
        total += 1


    for x in testInterFV.transpose():
        if normNat.likelihood(x) > normInter.likelihood(x):
            missclass += 1
        total += 1

    print(missclass/total)

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
